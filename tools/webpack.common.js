const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = (env) => ({
  entry: {
    app: path.resolve(__dirname, '../src/app/index.js'),
  },
  plugins: [
    new CleanWebpackPlugin(),
    new CopyPlugin({
      patterns: [
        {
          from: path.resolve(__dirname, '../public'),
          globOptions: { ignore: ['**/index.html', '**/assets/.gitkeep'] },
          noErrorOnMissing: true,
        },
      ],
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, '../public/index.html'),
      base: env.baseHref || '/',
    }),
  ],
  resolve: {
    alias: {
      assets: path.resolve(__dirname, '../public/assets'),
    },
  },
  module: {
    rules: [
      {
        test: /\.(png|svg|jpe?g|gif)$/,
        type: 'asset/resource',
        generator: {
          filename: (pathData) => `${pathData.filename.replace(/^public\//, '')}?${pathData.contentHash}`,
          emit: false,
        },
      },
    ],
  },
});
