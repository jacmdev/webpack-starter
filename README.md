# About

Project starter using Webpack and its ecosystem.

- Sass
- PostCSS Preset Env
- Babel
- Environment variables
- Source Maps
- Minification
- Live server
- EditorConfig
- Stylelint
- ESLint

# Installation

1. Clone repository
2. Install required packages

```
npm ci
```

# Use

## Watch

Tools will automatically watch for changes and rebuild project source files.

```
npm run watch
```

## Live server

Tools will automatically watch for changes, rebuild project source files and update page http://localhost:8080 in a web browser.

```
npm start
```

## Builds

Build project source files:

- For delevelopment
  - include source maps

```
npm run build
```

- For production
    - minification
    - file names with hash (cache busting)
    - PostCSS Preset Env
    - Babel
    - linters (ESLint, Stylelint)

```
npm run build:prod
```

# Project structure

- `public/` - files from this directory will be copied into `dist/`
- `public/assets/` - static assets (e.g. images)
- `public/index.html` - main HTML file
- `src/app/` - JavaScript files
- `src/app/index.js` - entry point for JavaScript files
- `src/environments/` - environment variables that can be used in JavaScript files
- `src/styles/index.scss` - entry point for styles
- `tools/` - webpack configurations for different environments
